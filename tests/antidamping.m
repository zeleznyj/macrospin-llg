%Ferromagnetic antidamping switching test

%define the model with uniaxial anistropy, antidamping torque and gilbert damping
m = model(1);
m.add_ani2(1,0.0001,[0 0 1]');
m.add_STT(1,0.02,[0 0 1]');
m.ag = 0.01;

%increase the accuracy of differential equation solver
params = llg_params();
params.relprec = 1e-6;

%solve llg
sol = m.solve_LLG(10,3*[0 sin(0.1) cos(0.1)]',params);

fprintf('Antidamping test\n')
fprintf('------------------\n')

%Test: norm of M(end)
fprintf('Norm test (relative error): %10.6e\n',norm(sol.M(end,:)-[0,0,-3])/3)

%Test: number of precessions
peaks_x = length(findpeaks(sol.M(:,1)));
peaks_y = length(findpeaks(sol.M(:,2)));
fprintf('Number of M_x precessions (correct value = 87): %i\n',peaks_x)   
fprintf('Number of M_y precessions (correct value = 88): %i\n',peaks_y)   

%Test: switching time
%fit M_z by splines to determine switching time
%we consider switching full when M_z = 2.999
f_Mz = @(t) spline(sol.T,sol.M(:,3),t)+2.999;
t_sw = fzero(f_Mz,1.5);
fprintf('Switching time (correct value= 2.103105): %10.6e\n',t_sw)

%Check that all tests have correct values
assert(norm(sol.M(end,:)-[0,0,-3])<1e-4,'Norm test failed')
assert(peaks_x == 87,'Number of M_x precessions test failed')
assert(peaks_y == 88,'Number of M_y precessions test failed')
assert(norm(t_sw-2.103105)<1e-3,'Switching time test failed')
