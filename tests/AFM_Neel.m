%Antiferromagnetic Field-like Neel order torque switching test

m = model(2);
m.add_exchange(0,0,0.1)
m.add_ani4(0,0.00001);
Bmag = 0.1;
m.add_B(1,Bmag*[0 1 0]');
m.add_B(2,-Bmag*[0 1 0]');
m.ag = 0.005;

%increase the accuracy of differential equation solver
params = llg_params();
params.relprec = 1e-6;

%Solve LLG
M0 = 3*[1 0 0 -1 0 0]';
sol = m.solve_LLG(0.02,M0,params);

fprintf('\n')
fprintf('AFM Neel torque test\n')
fprintf('----------------------\n')

%Test:norm of M1 and M2
norm_M1 = norm(sol.M(end,1:3) - [0 3 0]);
norm_M2 = norm(sol.M(end,4:6) - [0 -3 0]);
fprintf('Norm M1 (relative error): %10.6e\n',norm_M1/3)
fprintf('Norm M2 (relative error): %10.6e\n',norm_M1/3)

%Test: Switching time
nt = length(sol.T);
norms = zeros(nt,1);
for i=[1:nt]
    norms(i) = norm(sol.M(i,1:3)-[0 3 0]);
end

%Test: Precessions
%Checks heights and positions of first four peaks of abs(M1_x) and abs(M2_y)
%abs is used so that both negative and positive peaks are counted
T_int = linspace(0,0.01,10000);
Mx_int = spline(sol.T,sol.M(:,1),T_int);
My_int = spline(sol.T,sol.M(:,2),T_int);
[pks_x,loc_x] = findpeaks(abs(Mx_int));
[pks_y,loc_y] = findpeaks(abs(My_int));

Tloc_x = T_int(loc_x(1:4));
Tloc_y = T_int(loc_y(1:4));

pks_x_cor = [1.409098187856589e+00     7.388251469987968e-01     4.051191424252496e-01     2.246873014054526e-01];
Tloc_x_cor = [1.873187318731873e-03     3.141314131413141e-03     4.323432343234324e-03     5.484548454845485e-03];
pks_y_cor = [2.999908886224715e+00     2.999980279345079e+00     3.000000796684107e+00     3.000005316953166e+00];
Tloc_y_cor = [1.296129612961296e-03     2.615261526152616e-03     3.809380938093810e-03     4.974497449744974e-03];

pks_x_error = (pks_x(1:4)-pks_x_cor).^2./(pks_x_cor.^2);
Tloc_x_error = (Tloc_x(1:4)-Tloc_x_cor).^2./(Tloc_x_cor.^2);
pks_y_error = (pks_y(1:4)-pks_y_cor).^2./(pks_y_cor.^2);
Tloc_y_error = (Tloc_y(1:4)-Tloc_y_cor).^2./(Tloc_y_cor.^2);
prec_error=norm(pks_x_error)+norm(pks_y_error)+norm(Tloc_x_error)+norm(Tloc_y_error);

fprintf('M_x peaks height (relative error): %10.6e  %10.6e   %10.6e   %10.6e\n',pks_x_error)
fprintf('M_x peaks location (relative error): %10.6e  %10.6e   %10.6e   %10.6e\n',Tloc_x_error)
fprintf('M_y peaks height (relative error): %10.6e  %10.6e   %10.6e   %10.6e\n',pks_y_error)
fprintf('M_y peaks location (relative error): %10.6e  %10.6e   %10.6e   %10.6e\n',Tloc_y_error)
fprintf('Total prec error: %10.6e\n', prec_error)


assert(norm_M1<1e-4,'M1 norm test failed')
assert(norm_M2<1e-4,'M2 norm test failed')
assert(prec_error<1e-4,'Total precession error too large')
