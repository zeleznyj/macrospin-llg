classdef model < matlab.mixin.Copyable
%model for solving llg equations
%
%   Initialize by m = model(n_mag), where:
%       n_mag - number of magnetic moments
%
%   Add interactions to the Hamiltonian:
%   - m.add_exchange(a,b,J) -- adds exchange
%   - m.add_ani2(a,K,A) -- adds uniaxial anisotropy
%   - m.add_ani4(a,K) -- ads cubic anisotropy
%   - m.add_ani6(a,K,A) -- ads sixth order anisotropy 
%   - m.add_DMI(a,b,D) -- ads DMI
%   - m.add_B(a,B)
%   - m.add_Bm(a,f) -- ads effective field, which is a general function of m and t:
%       f(t,m) = B
%   - m.add_STT(a,S,p) -- adds Sloncewski-like STT torque
%
%   In all of these methods apart from DMI a and b can be:
%       - number of the magnetic moment
%       - 0: interaction is added for all magnetic moments
%       - row vector: then interaction is added for all elements of the vector
%   For DMI only the first option is available.
%   
%   Set damping by m.ag = ag
%   
%   Solve the model by sol = m.solve_LLG(tf,M0), where
%       tf - [0,tf] is the time interval used for the solution
%       M0 - is the column vector, which contains the initial magnetic 
%           moments in succession
%       this returns a solution object
%
%   The relative tolerance and absolute tolerance for the solution of LLG
%   can be modified by changing m.relprec and m.absprec.
%
%   Method find_Emin can be used to find magnetic ground states.
%
%   See also solution
    
    properties
        %these are basic parameters of the model that have to be set during
        %init
        n_mag
        %these are parameters that are given default values during init and
        %can be changed after
        J
        K2
        A2
        K6
        A6
        K4
        D
        ag
        B
        Bac
        Bac_f
        Bt
        STT
        Bmf
        Bmf2
    end

    properties (Constant)
        mu_B = 5.7883818012e-5 %eV/T
        gamma_e = 1.760859644e11 %1/sT
    end
    
    methods
        
        function self = model(n_mag)
            if n_mag > 0
                self.n_mag = int8(n_mag);
            else
                error('n_mag has to be positive')
            end
            self.J = zeros(3,3,n_mag,n_mag);
            self.K2 = zeros(n_mag,1);
            self.A2 = zeros(3,n_mag);
            self.K4 = zeros(n_mag,1);
            self.K6 = zeros(n_mag,1);
            self.A6 = zeros(3,n_mag);
            self.ag = 0;
            self.D = zeros(3,3,n_mag,n_mag);
            self.B = zeros(self.n_mag*3,1);
            self.Bac = zeros(self.n_mag*3,1);
            self.Bac_f = 0;
            self.Bt = @(t) 1;
            self.STT = zeros(3,n_mag);
            self.Bmf = cell(n_mag,1);
            self.Bmf2 = cell(n_mag,1);
            for a = [1:n_mag]
                self.Bmf{a} = 'N';
                self.Bmf2{a} = 'N';
            end
        end

        function add_exchange(self,a,b,J)
        %adds exchange between a and b
        %   Args:
        %       a (scalar or a row vector): first magnetic moment number
        %       b (scalar or a row vector): second magnetic moment number
        %       J (scalar or 3x3 symmetric matrix): the exchange - can be either number for isotropic 
        %       exchange or matrix 

            [n,m] = size(J);
            
            if isscalar(a) && isscalar(b) && a == b && a ~= 0
                error('Exchange only possible for different magnetic moments')
            end
            if n == 1 && m == 1 && isreal(J)
                Jm = diag([J J J]);
            elseif n == 3 && m == 3 && isreal(J) && J == J'
                Jm = J;
            else
                error('J must be either a real number or real symmetrical 3x3 matrix')
            end

            it1 = ita(self.n_mag,a);
            it2 = ita(self.n_mag,b);

            for a = it1
                for b = it2
                    if a ~= b
                        self.J(:,:,a,b) = Jm;
                    end
                end
            end

            if it1 ~= it2
                for a = it2
                    for b = it1
                        if a ~= b
                            self.J(:,:,a,b) = Jm;
                        end
                    end
                end
            end

        end
        
        function add_ani2(self,a,K,A)
        %adds uniaxial anisotropy
        %   Args:
        %       a (scalar or a row vector): magnetic moment number
        %       K (scalar): anisotropy constant
        %       A (column vector): uniaxial direction (doesn't have to be normalized)

            it = ita(self.n_mag,a);
            
            for a = it
                self.K2(a) = K;
                self.A2(:,a) = A/norm(A);
            end
        end

        function add_ani4(self,a,K)
        %adds cubic anisotropy of the form M_x^4+M_y^4+M_z^4
        %   Args:
        %       a (scalar or a row vector): magnetic moment number
        %       K (scalar): anisotropy constant

            it = ita(self.n_mag,a);

            for a = it
                self.K4(a) = K;
            end
        end

        function add_ani6(self,a,K,A)
        %adds sixth-order anisotropy of the form (MA)^6
        %   Args:
        %       a (scalar or a row vector): magnetic moment number
        %       K (scalar): anisotropy constant
        %       A (column vector): anisotropy direction (doesn't have to be normalized)

            it = ita(self.n_mag,a);

            for a = it
                self.K6(a) = K;
                self.A6(:,a) = A/norm(A);
            end
        end

        function add_B(self,a,B)
        %adds magnetic field
        %   Args:
        %       a (scalar or a row vector): magnetic moment number
        %       B (scalar): magnetic field (in T)

            it = ita(self.n_mag,a);

            for a = it
                [i1,i2] = i1i2(a);
                self.B(i1:i2) = B;
            end
        end

        function add_DMI(self,a,b,d)
        %adds DMI
        %   Args:
        %       a (scalar or a row vector): first magnetic moment number
        %       b (scalar or a row vector): second magnetic moment number
        %       B (column vector): magnetic field

            for i = 1:3
                for j = 1:3
                    for k = 1:3
                        self.D(j,k,a,b) = self.D(j,k,a,b) + d(i) * levi_civita([i j k]);
                    end
                end
            end
            self.D(:,:,b,a) = -self.D(:,:,a,b) 
        end

        function add_STT(self,a,S,p)
        %adds Sloncewski STT (only the torque no energy so far)
        %
        %   The torque is: MxB_eff, where B_eff = S(Mxp)/norm(M)/norm(p)
        %   Args:
        %       a (scalar or a row vector): first magnetic moment number
        %       S (scalar): the strength of the corresponding effective field in T
        %       p (column vector): "spin-polarization"
        
            it = ita(self.n_mag,a);
            for a = it
                self.STT(:,a) = p/norm(p) * S;
            end
        end

        function add_Bm(self,a,Bmf)
            %Adds an effective magnetic field which is a  general function of time and M
            %Args:
            %   a (scalar or a row vector): number of magnetic atom on which the field is acting
            %   Bmf(function): Bmf(t,M) returns a magnetic field acting on the atom in the unit of T  
            %       t is time and M is the magnetic moment on atom a.
            it = ita(self.n_mag,a);
            for a = it
                self.Bmf{a} = Bmf; 
            end
        end

        function add_Bm2(self,a,Bmf)
            %Adds an effective magnetic field which is a  general function of time and M
            %Unlike the function add_Bm this adds a function that takes as the argument 
            %all of the magnetic moments, not just the moment on atom a.
            %Args:
            %   a (scalar or a row vector): number of magnetic atom on which the field is acting
            %   Bmf(function): Bmf(t,M) returns a magnetic field acting on the atom in the unit of T  
            %       t is time and M is the array of all magnetic moments.
            it = ita(self.n_mag,a);
            for a = it
                self.Bmf2{a} = Bmf;
            end
        end

        function sol = solve_LLG(self,tf,M0,params)
        %solves the LLG equations
        %   Args:
        %       tf (scalar): [0,tf] is the time-interval used 
        %       M0 (column vector with length 9): starting magnetic configuraion in a column vector
        %       params (class(llg_params))(optional): This is a class that contains optional parame
        %           ters for the LLG equations
        %
        %   Returns:
        %       sol

            if exist('params') ~= 1
                params = llg_params();
                params.dM0 = zeros(self.n_mag*3,1);
            elseif params.dM0 == 0
                params.dM0 = zeros(self.n_mag*3,1);
            end
            nvar = self.n_mag*3;
            LLG_han = @(t,M,dM)self.LLG(t,M,dM);
            opt = odeset('RelTol', params.relprec,'AbsTol', params.absprec);
            [M0,dM0] = decic(LLG_han,params.t0,M0,ones(nvar,1),params.dM0,zeros(nvar,1),opt);
            [T,M] = ode15i(LLG_han,[params.t0 tf],M0,dM0,opt);
            sol = solution(self); %creates an empty solution object
            sol.T = T;
            sol.M = M;
        end
        
        function f = LLG(self,t,M,dM)
            f = zeros(self.n_mag*3,1);
            for n = 1:self.n_mag
                i1 = (n-1)*3+1;
                i2 = n*3;
                %LHS + anisotropy
                f(i1:i2) = dM(i1:i2)/(self.gamma_e*1e-9) - cross(M(i1:i2),self.B_ani(n,M));
                %damping
                f(i1:i2) = f(i1:i2)  + self.ag * cross(M(i1:i2)/norm(M(i1:i2)),(dM(i1:i2)/(self.gamma_e*1e-9)));
                %the exchange
                f(i1:i2) = f(i1:i2) - cross(M(i1:i2),self.B_ex(n,M));
                %DMI
                f(i1:i2) = f(i1:i2) - cross(M(i1:i2),self.B_DMI(n,M));
                %magnetic field
                f(i1:i2) = f(i1:i2) - cross(M(i1:i2),self.B(i1:i2)) * self.Bt(t);
                %AC magnetic field
                f(i1:i2) = f(i1:i2) - cross(M(i1:i2),self.Bac(i1:i2)) * cos(self.Bac_f*t);
                %STT
                f(i1:i2) = f(i1:i2) - cross(M(i1:i2),cross(M(i1:i2),self.STT(:,n)))/norm(M(i1:i2))*self.Bt(t);
                if isa(self.Bmf{n},'function_handle')
                    Bmf = self.Bmf{n};
                    f(i1:i2) = f(i1:i2) - cross(M(i1:i2),Bmf(t,M(i1:i2)));
                end
                if isa(self.Bmf2{n},'function_handle')
                    Bmf = self.Bmf2{n};
                    f(i1:i2) = f(i1:i2) - cross(M(i1:i2),Bmf(t,M));
                end
            end
        end
        
        function B_a = B_ani(self,n,M)
            i1 = (n-1)*3+1;
            i2 = n*3;
            M = M(i1:i2);
            B_a = zeros(3,1);
            %uniaxial anisotropy
            B_a = B_a + self.K2(n) * self.A2(:,n) * M'*self.A2(:,n) / (norm(M)^2);
            %cubic anisotropy
            B_a = B_a + 2 * self.K4(n) * M.^3 / norm(M)^4;
            %sixt-order
            B_a = B_a + 3/2 * self.K6(n) * self.A6(:,n) * (M'*self.A6(:,n))^5 / norm(M)^6;
            %unit
            B_a = B_a / self.mu_B;
        end 

        function B_J = B_ex(self,n,M)
            B_J = zeros(3,1);
            i1 = (n-1)*3+1;
            i2 = n*3;
            for m = 1:self.n_mag
                if n ~= m
                    j1 = (m-1)*3+1;
                    j2 = m*3;
                    %exchange
                    B_J = B_J - self.J(:,:,n,m) * M(j1:j2)...
                    / (norm(M(i1:i2))*norm(M(j1:j2))*self.mu_B);
                end
            end
        end

        function B_D = B_DMI(self,n,M)
            B_D = zeros(3,1);
            i1 = (n-1)*3+1;
            i2 = n*3;
            for m = 1:self.n_mag
                if n ~= m
                    j1 = (m-1)*3+1;
                    j2 = m*3;
                    %exchange
                    B_D = B_D - self.D(:,:,n,m) * M(j1:j2)...
                    / (norm(M(i1:i2))*norm(M(j1:j2))*self.mu_B);
                end
            end
        end


        function E = ene(self,t,M)
        %returns energy of magnetic configuration
        %   Args:
        %       t (scalar): time (only matters in case of field dependence
        %       M( column vector (length=3*n_mag)): column vector containing the
        %           magnetic configuration
        %   
        %   Returns:
        %       E (column vector): the energy
        %           E(1): total energy
        %           E(2): exchange energy
        %           E(3): DMI energy
        %           E(4): anisotropy energy
        %           E(5): energy due to magnetic field
            
            E = zeros(5,1);
            E(2) = self.J_energy(M);
            E(3) = self.DMI_energy(M);
            E(4) = self.ani_energy(M);
            E(5) = self.B_energy(t,M);
            E(1) = E(2) + E(3) + E(4) + E(5);
        end

        function E = ene1(self,t,M)
            Et = self.ene(t,M);
            E = Et(1);
        end

        function E = J_energy(self,M)
            E = 0;
            for i =1:self.n_mag
                i1 = (i-1)*3+1;
                i2 = i*3;
                for m = 1:self.n_mag
                    if i ~= m
                        j1 = (m-1)*3+1;
                        j2 = m*3;
                        E = E + M(i1:i2)'*self.J(:,:,i,m)/2* M(j1:j2) / (norm(M(i1:i2))*norm(M(j1:j2)));
                    end
                end
            end
        end

        function E = ani_energy(self,M)
            E = 0;
            for i = 1:self.n_mag
                i1 = (i-1)*3+1;
                i2 = i*3;
                %uniaxial
                E = E - self.K2(i) / 2 * (M(i1:i2)'*self.A2(:,i))^2 / norm(M(i1:i2))^2;
                %cubic
                E = E - self.K4(i) / 2 * sum((M.^4)) / norm(M)^4;
                %sixth-order
                E = E - self.K6(i) / 2 * (M(i1:i2)'*self.A6(:,i))^6 / norm(M(i1:i2))^6;
            end
            %cubic

        end

        function E = DMI_energy(self,M)
            E = 0;
            for i =1:self.n_mag
                i1 = (i-1)*3+1;
                i2 = i*3;
                for m = 1:self.n_mag
                    if i ~= m
                        j1 = (m-1)*3+1;
                        j2 = m*3;
                        E = E + M(i1:i2)'*self.D(:,:,i,m) / 2 * M(j1:j2) / (norm(M(i1:i2))*norm(M(j1:j2)));
                    end
                end
            end
        end

        function E = B_energy(self,t,M)
            E = - M' * self.mu_B * self.B * self.Bt(t);
        end

        %functions defined in a separate file
        M_min = find_Emin(self,M0)
    end
end

%auxiliary functions

function [i1,i2] = i1i2(n)
    i1 = (n-1)*3+1;
    i2 = n*3;
end

function [sa,fa] = safa(n_mag,a)
    if a == 0
        sa = 1;
        fa = n_mag;
    else
        sa = a;
        fa = a;
    end
end

function it = ita(n_mag,a)
    sa = size(a);
    if sa(1) ~= 1
        error('a must be either number or row vector')
    end

    if sa(2) == 1
        [sa,fa] = safa(n_mag,a);
        it = [sa:fa];
    else
        it = a;
    end
end

