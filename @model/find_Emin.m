function M_min = find_Emin(self,M0)
%finds minimum of energy near M0
%
%   Args:
%       M0: starting magnetic configuration
%
%   Returns:
%       M_min: magnetic configuration which minimizes the energy

    objfun = @(M) self.ene1(0,M);
    %objfun = @(M)0

    function [c,ceq] = constr(M)
        c = [];
        [nm,~] = size(M);
        ceq = zeros(nm,1);
        ceq(1:3) = norm(M0(1:3))-norm(M(1:3));
        ceq(4:6) = norm(M0(4:6))-norm(M(4:6));
        %ceq(7:9) = norm(M0(7:9))-norm(M(7:9));  
    end
    
    opt = optimset('Algorithm', 'interior-point')
    M_min = fmincon(objfun,M0,[],[],[],[],[],[],@constr,opt)

end
