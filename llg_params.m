classdef llg_params < handle

    properties
        relprec
        absprec
        dM0
        t0
    end

    methods
        function self = llg_params()
            self.relprec = 1e-3;
            self.absprec = 1e-6;
            self.dM0 = 0;
            self.t0 = 0;
        end
    end
end

